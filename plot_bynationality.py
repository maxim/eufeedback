import plotly.graph_objects as go
from loadutil import load_feedback, load_analysis
from collections import Counter
from typing import List, Dict
import json

feedback = load_feedback("feedback.jl")
data = load_analysis("feedback_annot.jl")
def load_country_codes():
    with open("countries.json", "r") as fd:
        countries_data = json.load(fd)
    return dict([(c["code"], c["label"]) for c in countries_data])
country_codes = load_country_codes()

count_country = Counter() # counter by country
count_cat = Counter() # counter by country and infavor

for id in feedback.keys():
    # To keep only feedback by EU Citizens, uncomment the lines
    #if feedback[id]["userType"] in ["EU_CITIZEN", "NON_EU_CITIZEN"]:
    #    continue
    country_code, infavor = feedback[id]["country"], data[id]["infavor"]
    if infavor == "no":
        print(id)
    #if country_code == "NLD" and infavor != "no":
    #    print(id)
    country = country_codes[country_code]
    count_country.update([country])
    count_cat.update([(country, infavor)])
print(count_country)

all_against = []
all_other   = []
all_infavor = []
all_countries = []
all_total = []

count_total = 0

for country, country_total in reversed(count_country.most_common()):
    count_against = count_cat[(country, "no")]
    count_other   = count_cat[(country, "unclear")] + count_cat[(country, "exclude")]
    count_infavor = count_cat[(country, "yes")]
    if country_total <= 1:
        print(f"{country}: {count_against}/{count_other}/{count_infavor}")
        continue
    count_total += country_total

    all_against.append(count_against)
    all_other.append(count_other)
    all_infavor.append(count_infavor)
    all_countries.append(country)
    all_total.append(country_total)

fig = go.Figure()
fig.add_trace(go.Bar(
    x=all_against, y=all_countries,
    text=[f"{v} ({v/all_total[idx]*100:.0f}%)" for idx, v in enumerate(all_against)],
    name="Against",
    orientation="h",
    marker=dict(color="red")
))
fig.add_trace(go.Bar(
    x=all_other, y=all_countries,
    text=[f"{v}" for idx, v in enumerate(all_other)],
    name="Other",
    orientation="h",
    marker=dict(color="gray")
))
fig.add_trace(go.Bar(
    x=all_infavor, y=all_countries,
    text=[f"{v} ({v/all_total[idx]*100:.0f}%)" for idx, v in enumerate(all_infavor)],
    name="In favor",
    orientation="h",
    marker=dict(color="lime")
))

fig.update_layout(title_text=f"Support by nationality<br>All nationalities with > 1 comment ({count_total} comments)", barmode="stack", font_size=16)
fig.update_xaxes(title = "Number of comments")
fig.write_image("bynationality.svg", width=1920, height=1080)
