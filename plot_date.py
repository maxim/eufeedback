import plotly.graph_objects as go
from loadutil import load_feedback, load_analysis
from collections import Counter
from datetime import datetime

feedback = load_feedback("feedback.jl")
data = load_analysis("feedback_annot.jl")

def parse_date(t : str):
    # format: "2022/09/07 14:09:06"
    return datetime(year=2022, month=int(t[5:7]), day=int(t[8:10]))

def plot_onebar(filterfunc, name, color):
    infavor_dates = Counter([parse_date(feedback[id]["dateFeedback"]) for id in feedback.keys() if filterfunc(data[id]["infavor"])])
    infavor_x = list(infavor_dates.keys())
    infavor_y = list(infavor_dates.values())
    return go.Bar(
        x=infavor_x, y=infavor_y, name=name, marker=dict(color=color)
    )


fig = go.Figure(data=[
    plot_onebar(lambda x: x == "no", "Against", "red"),
    plot_onebar(lambda x: x == "yes", "In favor", "lime"),
    plot_onebar(lambda x: x not in ["yes", "no"], "Other", "blue"),
    ])

fig.update_layout(barmode='stack')
fig.update_xaxes(
    ticks= "outside",
    ticklabelmode= "period", 
    tickcolor= "black",
    ticklen=20,  
    dtick=7*24*60*60*1000,  
    tick0="2022-05-13", 
    griddash='dot', 
    gridcolor='white',
    minor = dict(
        ticklen=5,  
        dtick=24*60*60*1000,  
        tick0="2022-05-13", 
    )
)

fig.update_layout(title_text="Comments by date and classification", font_size=28)
fig.update_yaxes(title="Number of comments per day")
fig.write_image("bydate.svg", width=1920, height=1080)
