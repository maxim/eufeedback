import json
def load_feedback(filename : str):
    data = {}
    with open(filename) as fd:
        lines = fd.readlines()
        for line in lines:
            entry = json.loads(line)
            assert entry["id"] not in data
            data[entry["id"]] = entry
    return data
def load_analysis(filename : str):
    data = {}
    with open(filename) as fd:
        lines = fd.readlines()
        for line in lines:
            entry = json.loads(line)
            assert entry["id"] not in data
            data[entry["id"]] = entry["analysis"]
    return data
