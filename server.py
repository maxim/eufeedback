from flask import Flask, url_for, redirect
from flask_restful import reqparse, Resource, Api
from loadutil import load_feedback, load_analysis
import json

print("Loading feedback...")
feedback = load_feedback("feedback.jl")
print("Loaded!")

app = Flask(__name__, static_folder="static", static_url_path='')

@app.route('/')
def redirect_index():
    return redirect("/index.html", code=302)


api = Api(app)

tags = load_analysis("feedback_annot.jl")
def get_remaining_ids():
    ids_avail = set(feedback.keys())
    ids_analyzed = set(tags.keys()) ^ set([(None if v["infavor"] is not None else k) for k,v in tags.items()])
    return (ids_avail ^ ids_analyzed) & ids_avail
print("To analyze:", get_remaining_ids())
def save_tags(tags):
    with open("feedback_annot.jl", "w") as fd:
        for id, entry in tags.items():
            json.dump({"id": id, "analysis": entry}, fd)
            fd.write("\n")

class FeedbackList(Resource):
    def get(self):
        return list(sorted(feedback.keys())), 200
api.add_resource(FeedbackList, "/allfeedback")

id_parser = reqparse.RequestParser()
id_parser.add_argument("id")
class Feedback(Resource):
    def post(self):
        args = analysis_parser.parse_args()
        id = int(args["id"])
        if id not in feedback:
            return {"status": "badid"}, 404
        return {"status": "ok", "data": feedback[id]}, 200
api.add_resource(Feedback, "/feedback")

def identity(x):
    return x
analysis_parser = reqparse.RequestParser()
analysis_parser.add_argument("id", type=int)
analysis_parser.add_argument("analysis", type=identity)
class FeedbackAnalysis(Resource):
    def get(self, id: int):
        if id not in tags:
            return {"status": "badid"}, 404
        print(tags[id])
        return tags[id]

    def post(self, id: int):
        args = analysis_parser.parse_args()
        if id not in feedback:
            return {"status": "badid"}, 404
        tags[id] = args["analysis"]
        save_tags(tags)
        return {"status": "saved"}, 200

api.add_resource(FeedbackAnalysis, "/analysis/<int:id>")

if __name__ == '__main__':
    app.run(debug=True)
