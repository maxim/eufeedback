.PHONY: all copy download
all: issues.svg sankey.svg bydate.svg bynationality.svg
download:
	mv feedback.jl feedback.jl.bak
	scrapy runspider eufeedback.py -o feedback.jl
download_countries:
	curl -o countries.json https://ec.europa.eu/info/law/better-regulation/brpapi/countries/
issues.svg: plot_criticisms.py feedback_annot.jl feedback.jl
	python3 plot_criticisms.py
sankey.svg: sankey.py feedback_annot.jl feedback.jl
	python3 sankey.py
bydate.svg: plot_date.py feedback_annot.jl feedback.jl
	python3 plot_date.py
bynationality.svg: plot_bynationality.py feedback_annot.jl feedback.jl
	python3 plot_bynationality.py
