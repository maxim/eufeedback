var feedbackids;
var theform = document.forms[0];
var currentidx = 0;
async function get_ids(){
    const res = await fetch("/allfeedback");
    window.feedbackids = await res.json();
    let currentid;
    try {
        currentid = parseInt(document.location.hash.substring(1)) || -1;
        currentidx = window.feedbackids.indexOf(currentid);
        if(currentidx == -1) {
            currentidx = 0;
            currentid = window.feedbackids[0];
        }
    } catch(e) {
        currentid = window.feedbackids[0];
    }
    load_feedback(currentid);
}
function show_attachments(entry) {
    let attachmentsnode = document.querySelector("#attachmentslinks");
    attachmentsnode.innerHTML = "";
    for(const attachment of entry.data.attachments) {
        let el = document.createElement("a");
        el.innerText = attachment.ersFileName;
        el.href = `https://ec.europa.eu/info/law/better-regulation/api/download/${attachment.documentId}`;
        attachmentsnode.appendChild(el);
    }
}
async function load_feedback(id) {
    document.querySelector("#status").innerText = "Not saved";
    document.location.hash = id;
    let entry = await (await fetch("/feedback", { method: "POST", body: JSON.stringify({id: id}), headers: new Headers({'content-type': 'application/json'}) })).json();
    console.log(entry);
    document.querySelector("#feedback-content").innerText = entry.data.feedback;
    document.querySelector("#feedback-id").innerText = entry.data.id;
    document.querySelector("#feedback-usertype").innerText = entry.data.userType;
    show_attachments(entry);
    clear_all_inputs();
    load_analysis(id);
}
async function load_analysis(id) {
    let analysis = await (await fetch(`/analysis/${id}`, { method: "GET", headers: new Headers({'content-type': 'application/json'}) })).json();
    theform["infavor"].value = analysis.infavor;
    theform["mark"].checked = analysis.marked;
    theform["needtranslation"].checked = analysis.needtranslation;
    theform["ineffective"].checked = analysis.ineffective;
    theform["expansion"].checked = analysis.expansion;
    theform["accuracy"].checked = analysis.accuracy;
    theform["privacy"].checked = analysis.privacy;
    theform["alternative"].checked = analysis.alternative;
    theform["notes"].value = analysis.notes || "";
}

function disable_all_inputs() {
    theform.querySelectorAll("fieldset").forEach(e => e.setAttribute("disabled", ""))
}
function enable_all_inputs() {
    theform.querySelectorAll("fieldset").forEach(e => e.removeAttribute("disabled"))
}
function clear_all_inputs() {
    theform.reset();
}
async function next_feedback() {
    currentidx += 1;
    if(currentidx >= window.feedbackids.length) {
        currentidx = 0;
    }
    let id = window.feedbackids[currentidx];
    await load_feedback(id);
}
async function previous_feedback() {
    currentidx -= 1;
    if(currentidx < 0) {
        currentidx = window.feedbackids.length-1;
    }
    let id = window.feedbackids[currentidx];
    await load_feedback(id);
}
async function save_analysis() {
    document.querySelector("#status").innerText = "Saving...";
    let analysis = {
        "infavor": theform["infavor"].value || null,
        "marked": theform["mark"].checked,
        "needtranslation": theform["needtranslation"].checked,
        "ineffective": theform["ineffective"].checked,
        "expansion": theform["expansion"].checked,
        "accuracy": theform["accuracy"].checked,
        "privacy": theform["privacy"].checked,
        "alternative": theform["alternative"].checked,
        "notes": theform["notes"].value || "",
    };
    let id = window.feedbackids[currentidx]
    let data = {
        "analysis": analysis,
    };
    console.log(`Saving feedback id ${id} (nr ${currentidx})`);
    let saved = await (await fetch(`/analysis/${id}`, { method: "POST", body: JSON.stringify(data), headers: new Headers({'content-type': 'application/json'}) })).json();
    document.querySelector("#status").innerText = "Saved!";
}
document.querySelector("#next").onclick = next_feedback;
document.querySelector("#previous").onclick = previous_feedback;
document.querySelector("#save").onclick = save_analysis;

get_ids();
