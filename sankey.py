import plotly.graph_objects as go
from dataclasses import dataclass
from loadutil import load_feedback, load_analysis
from typing import List, Optional
import json

DEFAULT_NODE_COLOR = "black"
@dataclass
class Node:
    label: str
    column: int
    id: int
    color : str = DEFAULT_NODE_COLOR
class Links:
    def __init__(self):
        self.labels: List[str] = []
        self.sources: List[int] = []
        self.targets: List[int] = []
        self.values: List[int] = []
        self.link_colors: List[str] = []
        self.nodes: List[Node] = []

    def node(self, label, column) -> Node:
        self.labels.append(label)
        n = Node(label=label, column=2 if column is None else column, id=len(self.nodes)) 
        self.nodes.append(n)
        return n
    @property
    def node_colors(self):
        return [n.color for n in self.nodes]
    @property
    def node_columns(self):
        return [n.column for n in self.nodes]

    def add_link(self, source : Node, target : Node, value : int, link_color : str = "lightgray"):
        self.sources.append(source.id)
        self.targets.append(target.id)
        self.values.append(value)
        self.link_colors.append(link_color)
    def add_dummy_node(self, source : Node):
        self.sources.append(source.id)
        self.targets.append(len(self.nodes))
        self.values.append(0)
        self.link_colors.append("lightgray")

feedback = load_feedback("feedback.jl")
data = load_analysis("feedback_annot.jl")
# To filter data, uncomment the following lines
# Ensure counts below (count_*) are > 0, otherwise
# the layout gets messed up
#data = dict((k,v) for k,v in data.items() if feedback[k]["country"] != "DEU")
#feedback = dict((k,v) for k,v in feedback.items() if feedback[k]["country"] != "DEU")
count_against = sum(item["infavor"] == "no" for item in data.values())
count_infavor = sum(item["infavor"] == "yes" for item in data.values())
count_unclear = sum(item["infavor"] == "unclear" for item in data.values())
count_excluded = sum(item["infavor"] == "exclude" for item in data.values())

FIRST_COLUMN = 0.15
SECOND_COLUMN = 0.4
THIRD_COLUMN = 0.85

l = Links()
NODE_TOTAL = l.node(f"Total: {len(data)} comments", FIRST_COLUMN)
NODE_AGAINST = l.node(f"Against proposal ({count_against})", SECOND_COLUMN)
NODE_INFAVOR = l.node(f"In favor of proposal ({count_infavor})", SECOND_COLUMN)
NODE_UNCLEAR = l.node(f"Unclear position ({count_unclear})", SECOND_COLUMN)
NODE_EXCLUDED = l.node(f"Excluded ({count_excluded})", SECOND_COLUMN)
print(f"{count_excluded=}")
l.add_link(NODE_TOTAL, NODE_AGAINST, count_against)
NODE_AGAINST.color = "red"
l.add_link(NODE_TOTAL, NODE_INFAVOR, count_infavor)
NODE_INFAVOR.color = "lime"
l.add_link(NODE_TOTAL, NODE_UNCLEAR, count_unclear)
NODE_EXCLUDED.color = "gray"
l.add_link(NODE_TOTAL, NODE_EXCLUDED, count_excluded)
print(l.node_columns)

ypos = dict([(x,0.1) for x in range(5)]) # reserved ypos
for (lbl, usertype) in [("EU Citizen", "EU_CITIZEN"), ("Non-EU Citizen", "NON_EU_CITIZEN"), ("Other", "OTHER"), ("NGO", "NGO"), ("Company", "COMPANY"), ("Public Authority", "PUBLIC_AUTHORITY"), ("Business Association", "BUSINESS_ASSOCIATION")]:
    count_sub_against = sum(item["infavor"] == "no" and feedback[id]["userType"] == usertype for id,item in data.items())
    count_sub_infavor = sum(item["infavor"] == "yes" and feedback[id]["userType"] == usertype for id,item in data.items())
    if count_sub_against != 0:
        NODE_SUB_AGAINST = l.node(f"{lbl}- ({count_sub_against})", THIRD_COLUMN)
        NODE_SUB_AGAINST.color = "red"
        l.add_link(NODE_AGAINST, NODE_SUB_AGAINST, count_sub_against)
        ypos[NODE_SUB_AGAINST.id] = 0.1
    if count_sub_infavor != 0:
        NODE_SUB_INFAVOR = l.node(f"{lbl}+ ({count_sub_infavor})", THIRD_COLUMN)
        NODE_SUB_INFAVOR.color = "lime"
        l.add_link(NODE_INFAVOR, NODE_SUB_INFAVOR, count_sub_infavor)
        ypos[NODE_SUB_INFAVOR.id] = 0.5

    
for source, target, value in zip(l.sources, l.targets, l.values):
    print(f"{l.labels[source]} => {l.labels[target]}: {value}")

y = [pos[1] for pos in sorted(ypos.items())] # get value
assert len(l.labels) == len(l.node_columns)
l.add_dummy_node(NODE_AGAINST)
print(l.node_columns)
print(y)
#print([0.1] * 5 + [x/(len(l.labels)-5)*2.9+0.15 for x in range(len(l.labels)-5)])
fig = go.Figure(data=[go.Sankey(
    arrangement="snap",
    node = dict(
      pad = 25,
      thickness = 20,
      line = dict(color = "black", width = 0.5),
      label = l.labels,
      color = l.node_colors,
      x = l.node_columns,
      y = y
    ),
    link = dict(
      source = l.sources, # indices correspond to labels, eg A1, A2, A1, B1, ...
      target = l.targets,
      value =  l.values,
      color = l.link_colors
  ))])

fig.update_layout(title_text="Support of the proposal", font_size=32)
fig.write_image("sankey.svg", width=1920, height=1080)
