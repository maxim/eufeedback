import plotly.graph_objects as go
from loadutil import load_feedback, load_analysis
from typing import List, Dict

feedback = load_feedback("feedback.jl")
data = load_analysis("feedback_annot.jl")

against = list(filter(lambda x: x["infavor"] == "no", data.values()))
def count(data: List[Dict], field : str):
    return sum(x[field] for x in data)
ineffective_count = count(against, "ineffective")
expansion_count = count(against, "expansion")
accuracy_count = count(against, "accuracy")
privacy_count = count(against, "privacy")
alternative_count = count(against, "alternative")
nothing_count = sum(not (x["ineffective"] or x["expansion"] or x["privacy"] or x["accuracy"] or x["alternative"]) for x in against)

x = list(reversed([ineffective_count, expansion_count, accuracy_count, privacy_count, alternative_count, nothing_count]))
y = list(reversed(["Ineffective/Disproportionate", "Fear expansion/abuse", "Accuracy issues", "Privacy concerns", "Propose alternative", "None (Simple oppose)"]))
text = [f"{xval} ({xval/len(against)*100:.1f}%)" for xval in x]
fig = go.Figure(data=[go.Bar(
    x=x, y=y, text=text,
    orientation="h"
    )])

fig.update_xaxes(range=[0, len(against)])

fig.update_layout(title_text=f"Common criticisms by opponents (n={len(against)})", font_size=32)
fig.update_xaxes(title = "Number of comments<br>Multiple choices possible, except for simple oppose")
fig.write_image("issues.svg", width=1920, height=1080)
