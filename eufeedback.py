import scrapy
import json


class EufeedbackSpider(scrapy.Spider):
    name = 'eufeedback'
    allowed_domains = ['ec.europa.eu']
    start_urls = ['https://ec.europa.eu/info/law/better-regulation/brpapi/allFeedback?publicationId=30786148&page=0&size=10']

    def parse(self, response):
        data = json.loads(response.text)
        # yield each record separately for correct storing
        for feedback in data["_embedded"]["feedback"]:
            yield feedback
        if "next" in data["_links"]:
            next_page_num = data["page"]["number"] + 1
            next_page = f"https://ec.europa.eu/info/law/better-regulation/brpapi/allFeedback?publicationId=30786148&page={next_page_num}&size=10"
            # add more to scrape to the queue
            yield scrapy.Request(next_page, callback=self.parse)
